# 腾讯地图小demo

> 地图点击获取位置经纬度，输入位置进行匹配跳转

<video width=100% controls="controls" src="/doc/2020-05-11.mp4">

<img src="/doc/05-11.jpg" alt="show" />


### 注意，由于跨域原因，该示例需要与服务器端一起使用

#### vue代码
```
1. index.html中引入
<script charset="utf-8" src="https://map.qq.com/api/js?v=2.exp&key=ZWZBZ-Q7LWF-OM3JC-JFSUO-HW7MJ-FKFLH"></script>
key 需要自己申请
2. 由于是外部库，需要在vue.config.js中配置externals引入
configureWebpack: config => {
	config.externals = {
		qq: 'qq',
		TMap: 'TMap',
	}
},
此次是vue-cli4生成的项目，其他版本或者有webpack文件的自行搜索
3. 引入一个axios，请求服务器端接口
```

#### 4. 编写具体的地图代码
```
<template>
  <div>
    <input v-model="key" type="text" />
    <button @click="search">search</button>

    <div>
      经度 - {{ longitude }}
      纬度 - {{ latitude }}
    </div>
    <div id="container" style="width:600px;height:500px;"></div>
  </div>
</template>

<script type="model">

import qq from 'qq'


export default {
  mounted() {
    console.log()

    this.init();
  },
  data() {
      return {
        map: {},
        key: '',
        longitude:39.916527,//经度
        latitude:116.397128//纬度
      }
  },
  
  methods:{
    search(){
      this.$axios({
        method:"get",
        // 该url为服务器端地址
        url:"http://localhost:9527/map",
        dataType: "jsonp",
        params:{
          name: this.key,
        },
      }).then((ok)=>{
        console.log(ok.data.result.location)
        this.latitude = ok.data.result.location.lat
        this.longitude = ok.data.result.location.lng
        let lat = new qq.maps.LatLng(this.latitude, this.longitude)
        this.map.panTo(lat)
      },(err)=>{
        console.log(err)
      })
    },
    init() {
      let that = this
      //步骤：定义map变量 调用 qq.maps.Map() 构造函数   获取地图显示容器
      //设置地图中心点
      var myLatlng = new qq.maps.LatLng(this.longitude,this.latitude);
      //定义工厂模式函数
      var myOptions = {
        zoom: 8,               //设置地图缩放级别
        center: myLatlng,      //设置中心点样式
        mapTypeId: qq.maps.MapTypeId.ROADMAP  //设置地图样式详情参见MapType
      }
      //获取dom元素添加地图信息
      this.map = new qq.maps.Map(document.getElementById("container"), myOptions);
      //给地图添加点击事件
      //并获取鼠标点击的经纬度
      qq.maps.event.addListener(this.map, 'click', function(event) {
          that.changeLat(event.latLng.getLat())
          that.changeLon(event.latLng.getLng())
      });
    },
    changeLon(e){
      this.longitude = e
    },
    changeLat(e){
      this.latitude = e
    },
  }
}
</script>

<style>

</style>


```

### java代码
```
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Description: 腾讯地图api
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-05-11 10:20
 **/
@RestController
@RequestMapping("/map")
public class MapController {
	
	private RestTemplate restTemplate = new RestTemplate();
	
	@RequestMapping("")
	@CrossOrigin(origins = "*")
	public String restTemplateDemo(@RequestParam String name){
		try {
			//跨域访问表头
			HttpHeaders headers = new HttpHeaders();
			//接口地址
			String url = "https://apis.map.qq.com/ws/geocoder/v1?address=" + name + "&key=ZWZBZ-Q7LWF-OM3JC-JFSUO-HW7MJ-FKFLH";
			//restTemplate的POST请求，必须是使用MultiValueMap
			MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
			//利用multiValueMap插入需要传输的数据
//			multiValueMap.add("id","1");
//			multiValueMap.add("name","小明");
			//放入Http传输的数据
			HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(multiValueMap,headers);
			//访问接口并获取返回值
			ResponseEntity<String> responseEntity = restTemplate.getForEntity(url,String.class);
			//输出接口所返回过来的值
			return responseEntity.getBody();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
		
		}
		return null;
	}

}
```